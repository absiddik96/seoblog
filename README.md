## Project Setup
## Frontend 
- cd frontend
- cp next.config-example.js next.config.js 
- npm install
- npm run dev

## Backend
Open another terminal for backend
- cd backend
- cp .env.example .env
- set **Mongodb** database url in **DATABASE** in **.env** to connect into the database
- set 32 digit **Secrete key** in **JWT_SECRET** for JWT encrypted authentication token in **.env**
- npm install
- npm start
