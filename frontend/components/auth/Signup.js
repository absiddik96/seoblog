import {Fragment, useState, useEffect} from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import cn from 'classnames';
import {isAuth, signup} from "../../actions/auth";
import {toast} from "../../actions/alert";
import Router from 'next/router';

const Signup = () => {
  const [values, setValues] = useState({
    loading: false,
    showForm: true
  });
  
  const schema = yup.object().shape({
    name: yup.string().required('Name is required.'),
    email: yup.string().email().required('Email is required.'),
    password: yup.string().required('Password is required.').min(6),
    confirm_password: yup.string().oneOf([yup.ref('password'), null], 'Password not match')
  });
  
  useEffect(() => {
    if (isAuth()) {
      Router.push('/')
    }
  }, []);
  
  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema)
  });
  const onSubmit = async data => {
    setValues({
      ...values,
      loading: true
    });
    const res = await signup(data);
    if (res.status === 200){
      toast(res.data.msg);
      await Router.push('/sign-in')
    } else if (res.status === 400) {
      setError(res.data.error.param, {
        message: res.data.error.msg,
      });
    } else if (res.status === 500) {
      toast(res.data.error.msg, 'warning');
    }
    setValues({
      ...values,
      loading: false
    })
  };
  
  const SignupForm = () => {
    return (
      <div className="card">
        <div className="card-header">
          <h3>Sign Up</h3>
        </div>
        <div className="card-body">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className={cn('form-control', {'is-invalid': errors.name})} id="name" name="name" ref={register}/>
              {errors.name && <p className="invalid-feedback">{errors.name.message}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input type="email" className={cn('form-control', {'is-invalid': errors.email})} id="email" name="email" ref={register}/>
              {errors.email && <p className="invalid-feedback">{errors.email.message}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type="password" className={cn('form-control', {'is-invalid': errors.password})} id="password" name="password" ref={register}/>
              {errors.password && <p className="invalid-feedback">{errors.password.message}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="confirm_password">Confirm Password</label>
              <input type="password" className={cn('form-control', {'is-invalid': errors.confirm_password})} id="confirm_password" name="confirm_password" ref={register}/>
              {errors.confirm_password && <p className="invalid-feedback">{errors.confirm_password.message}</p>}
            </div>
            <div className="form-group">
              <button className="btn btn-sm btn-outline-info">Submit</button>
            </div>
          </form>
        </div>
      </div>
    )
  };
  
  return (
    <Fragment>
      {SignupForm()}
    </Fragment>
  );
};

export default Signup;