import {useEffect} from 'react';
import Router from 'next/router';
import {isAdmin, isAuth} from '../../actions/auth';

const Admin = ({ children }) => {
  useEffect(() => {
    if (!isAuth()) {
      Router.push('/sign-in');
    } else if (!isAdmin()) {
      Router.push('/');
    }
  }, []);
  return (
    <React.Fragment>
      {children}
    </React.Fragment>
  );
};

export default Admin;