import {Fragment, useEffect} from 'react';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers';
import * as yup from "yup";
import cn from 'classnames';
import {authenticate, isAdmin, isAuth, signin} from "../../actions/auth";
import {toast} from "../../actions/alert";
import Router from "next/router";

const Signin = () => {
  const schema = yup.object().shape({
    email: yup.string().email().required('Email is required.'),
    password: yup.string().required('Password is required.')
  });
  
  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema)
  });
  
  useEffect(() => {
    if (isAuth()) {
      if (isAdmin()) {
        Router.push('/admin');
      } else {
        Router.push('/user');
      }
    }
  }, []);
  
  const onSubmit = async data => {
    const res = await signin(data);
    if (res.status === 200) {
      toast('Sign In successfully');
      authenticate(res.data, () => {
        if (isAuth()) {
          if (isAdmin()) {
            Router.push('/admin');
          } else {
            Router.push('/user');
          }
        }
      })
    } else if (res.status === 400) {
      setError(res.data.error.param, {
        message: res.data.error.msg,
      });
    } else if (res.status === 500) {
      toast(res.data.error.msg, 'warning');
    }
  };
  
  const SigninForm = () => {
    return (
      <div className="card">
        <div className="card-header">
          <h3>Sign In</h3>
        </div>
        <div className="card-body">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input type="email" className={cn('form-control', { 'is-invalid': errors.email })} id="email" name="email" ref={register}/>
              {errors.email && <p className="invalid-feedback">{errors.email.message}</p>}
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type="password" className={cn('form-control', { 'is-invalid': errors.password })} id="password" name="password" ref={register}/>
              {errors.password && <p className="invalid-feedback">{errors.password.message}</p>}
            </div>
            <div className="form-group">
              <button className="btn btn-sm btn-outline-info">Submit</button>
            </div>
          </form>
        </div>
      </div>
    )
  };
  
  return (
    <Fragment>
      {SigninForm()}
    </Fragment>
  );
};

export default Signin;