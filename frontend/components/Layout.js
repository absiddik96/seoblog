import Head from "next/head";
import {APP_NAME} from "../config";
import Header from "./Header";
import Footer from "./Footer";

const Layout = ({ children }) => {
  const head = () => (
    <Head>
      <title>{APP_NAME}</title>
      <meta name="description" content="Programing blog"/>
    </Head>
  )

  return (
    <React.Fragment>
      {head()}
      <Header/>
      <div className="container my-4 pt-5">
        {children}
      </div>
      <Footer/>
    </React.Fragment>
  );
};

export default Layout;