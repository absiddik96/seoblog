import {Fragment, useState} from 'react';
import {APP_NAME} from "../config";
import Link from "next/link"
import {Collapse, Navbar, NavbarToggler, Nav, NavItem, NavLink, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown} from 'reactstrap';
import {isAdmin, isAuth, signOut} from "../actions/auth";
import Router from 'next/router';
import NProgress from 'nprogress';
import '.././node_modules/nprogress/nprogress.css';
import '.././node_modules/suneditor/dist/css/suneditor.min.css';

Router.onRouteChangeStart = url => NProgress.start();
Router.onRouteChangeComplete = url => NProgress.done();
Router.onRouteChangeError = url => NProgress.done();

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  
  const toggle = () => setIsOpen(!isOpen);
  
  return (
    <div>
      <Navbar className="fixed-top" color="light" light expand="md">
        <Link href="/">
          <NavLink className="navbar-brand">{APP_NAME}</NavLink>
        </Link>
        <NavbarToggler onClick={toggle}/>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <Link href="/">
                <NavLink>Home</NavLink>
              </Link>
            </NavItem>
            {!isAuth() && (
              <Fragment>
                <NavItem>
                  <Link href="/sign-up">
                    <NavLink>Sign Up</NavLink>
                  </Link>
                </NavItem>
                <NavItem>
                  <Link href="/sign-in">
                    <NavLink>Sign In</NavLink>
                  </Link>
                </NavItem>
              </Fragment>
            )}
            {isAuth() && !isAdmin() && (
              <Fragment>
                <NavItem>
                  <Link href="/user">
                    <NavLink>Dashboard</NavLink>
                  </Link>
                </NavItem>
              </Fragment>
            )}
            {isAdmin() && (
              <Fragment>
                <NavItem>
                  <Link href="/admin">
                    <NavLink>Dashboard</NavLink>
                  </Link>
                </NavItem>
                <NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Article Management
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem onClick={() => Router.replace('/admin/category')}>
                        Category
                      </DropdownItem>
                      <DropdownItem onClick={() => Router.replace('/admin/tag')}>
                        Tag
                      </DropdownItem>
                      <DropdownItem onClick={() => Router.replace('/admin/blog')}>
                        Blog
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </NavItem>
              </Fragment>
            )}
            {isAuth() && (
              <NavItem>
                <NavLink onClick={() => signOut(() => Router.replace('/'))}>Sign Out</NavLink>
              </NavItem>
            )}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Header;