import {useState, useEffect} from 'react';
import Link from "next/link";
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import {list, remove} from "../../../actions/blog";

const BlogIndex = () => {
    const [blogs, setBlogs] = useState([]);
    const [reload, setReload] = useState(false);
    useEffect(() => {
        list().then(blogs => setBlogs(blogs));
    }, [reload]);
    
    const [deleteableBlog, setDeleteableBlog] = useState(null);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const deleteToggle = () => setDeleteModalOpen(!deleteModalOpen);
    
    const makeDeleteable = async (blog) => {
        await setDeleteModalOpen(true);
        await setDeleteableBlog(blog);
    };
    
    const deleteConfirm = async (blog) => {
        await setDeleteModalOpen(false);
        await remove(blog._id);
        setReload(!reload);
    };
    
    
    return (
        <div className="row">
            <Modal isOpen={deleteModalOpen}>
                <ModalHeader>
                    <i className="fa fa-trash"></i> Delete Confirmation?
                </ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <p>Are you sure you want to delete this?</p>
                    </div>
                    <div className="form-group float-right">
                        <button type="button" className="btn btn-sm btn-outline-danger" onClick={deleteToggle}> <i className="fa fa-times"></i> No</button>{' '}
                        <button className="btn btn-sm btn-outline-info" onClick={() => deleteConfirm(deleteableBlog)}> <i className="fa fa-check"></i> Yes</button>
                    </div>
                </ModalBody>
            </Modal>
            <div className="col-md-12">
                <div className="card">
                    <div className="card-header">
                        <i className="fa fa-list"/> Blog List
                        <Link href="/admin/blog/create">
                            <a className="btn btn-sm btn-outline-info float-right">
                                <i className="fa fa-plus"/> {' '}Add new
                            </a>
                        </Link>
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {blogs.length > 0 && blogs.map((blog, index) => {
                                    return (
                                      <tr key={index}>
                                          <td>{index + 1}</td>
                                          <td>{blog.title}</td>
                                          <td>
                                              <button className="btn btn-outline-info">
                                                  <i className="fa fa-edit"></i>
                                              </button>
                                              {' '}
                                              <button onClick={() => makeDeleteable(blog)} className="btn btn-outline-danger">
                                                  <i className="fa fa-trash"></i>
                                              </button>
                                          </td>
                                      </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BlogIndex;