import {Fragment, useState, useEffect} from 'react';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers';
import * as yup from "yup";
import cn from 'classnames';
import Router from "next/router";
import {create} from "../../../actions/blog";
import {list as getCategories} from '../../../actions/category';
import {list as getTags} from '../../../actions/tag';
import {toast} from "../../../actions/alert";
import {withRouter} from 'next/router';
import Link from "next/link";
import SunEditor from 'suneditor-react';
import {options} from "../../../helpers/suneditor";

const BlogCreate = ({router}) => {
    const [values, setValues] = useState({
        loading: false,
        reload: false,
    });

    useEffect(() => {
        initCategories();
        initTags();
    }, [router]);

    const [categories, setCategories] = useState([]);
    const [tags, setTags] = useState([]);

    const initCategories = () => {
        getCategories().then(data => {
            if (data.error) {
                setValues({...values, error: data.error});
            } else {
                setCategories(data);
            }
        });
    };

    const initTags = () => {
        getTags().then(data => {
            if (data.error) {
                setValues({...values, error: data.error});
            } else {
                setTags(data);
            }
        });
    };

    const showCategories = () => {
        return (
            categories &&
            categories.map((c, i) => (
                <li key={i} className="list-unstyled">
                    <input name="categories" value={c._id} type="checkbox" className="mr-2" ref={register}/>
                    <label className="form-check-label">{c.name}</label>
                </li>
            ))
        );
    };

    const showTags = () => {
        return (
            tags &&
            tags.map((t, i) => (
                <li key={i} className="list-unstyled">
                    <input name="tags" value={t._id} type="checkbox" className="mr-2" ref={register}/>
                    <label className="form-check-label">{t.name}</label>
                </li>
            ))
        );
    };

    const FILE_SIZE = 1024 * 1024;
    const SUPPORTED_FORMATS = [
        "image/jpg",
        "image/jpeg",
        "image/gif",
        "image/png"
    ];
    const schema = yup.object().shape({
        title: yup.string().required('Title is required.'),
        body: yup.string().required('Body is required.'),
        categories: yup.string().required('Category is required.'),
        tags: yup.string().required('Tag is required.'),
        photo: yup.mixed().test('required', "Photo is required.",
            value => value[0]
        ).test("fileFormat", "Unsupported Format",
            value => value[0] && SUPPORTED_FORMATS.includes(value[0].type)
        ).test("fileSize", "File too large",
            value => value[0] && value[0].size <= FILE_SIZE
        )
    });

    const {register, handleSubmit, errors, setError, reset, setValue} = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = async data => {
        setValues({
            ...values,
            loading: true
        });
        const res = await create(data);
        if (res.status === 200) {
            reset();
            toast(res.data.msg);
            await Router.push('/admin/blog')
        } else if (res.status === 400) {
            setError(res.data.error.param, {
                message: res.data.error.msg,
            });
        } else if (res.status === 500) {
            toast(res.data.error.msg, 'warning');
        }
        setValues({
            ...values,
            loading: false,
            reload: !values.reload
        });
    };
    
    useEffect(() => {
        register({ name: "body" });
    }, [register]);
    
    const [body, setBody] = useState('');
    const handleBody = (e) => {
        setBody(e);
    };
    
    useEffect(() => {
        if (body === '<p><br></p>') {
            setValue('body', '');
        } else {
            setValue('body', body);
        }
    }, [body]);

    const blogForm = () => {
        return (
            <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">
                            <b>
                                <i className="fa fa-plus"></i> {' '}Add new blog
                                <Link href="/admin/blog">
                                    <a className="btn btn-sm btn-outline-info float-right">
                                        <i className="fa fa-arrow-left"/> {' '}Back
                                    </a>
                                </Link>
                            </b>
                        </div>
                        <div className="card-body">
                            <form onSubmit={handleSubmit(onSubmit)} encType="multi">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label htmlFor="title">Title</label>
                                            <input type="text" className={cn('form-control', {'is-invalid': errors.title})} id="title" name="title" ref={register}/>
                                            {errors.title && <p className="invalid-feedback">{errors.title.message}</p>}
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="body">Body</label>
                                            <SunEditor onChange={handleBody} setOptions={options}/>
                                            {errors.body && <p className="error">{errors.body.message}</p>}
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="photo">Photo</label>
                                            <input type="file" className={cn('form-control-file', {'is-invalid': errors.photo})} id="photo" name="photo" ref={register}/>
                                            {errors.photo && <p className="invalid-feedback">{errors.photo.message}</p>}
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label htmlFor="categories">Category</label>
                                            {errors.categories && <p className="error">{errors.categories.message}</p>}
                                            <hr className="mt-0"/>
                                            <ul style={{maxHeight: '200px', overflowY: 'scroll'}}>{showCategories()}</ul>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="tags">Tag</label>
                                            {errors.tags && <p className="error">{errors.tags.message}</p>}
                                            <hr className="mt-0"/>
                                            <ul style={{maxHeight: '200px', overflowY: 'scroll'}}>{showTags()}</ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-sm btn-outline-info float-right">
                                        <i className="fa fa-check"></i> {' '}Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    return (
        <Fragment>
            {blogForm()}
        </Fragment>
    );
};

export default withRouter(BlogCreate);