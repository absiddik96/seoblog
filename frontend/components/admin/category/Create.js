import {Fragment, useState, useEffect} from 'react';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers';
import * as yup from "yup";
import cn from 'classnames';
import Router from "next/router";
import {create, list, remove, update} from "../../../actions/category";
import {toast} from "../../../actions/alert";
import CategoryIndex from "./index";

const CategoryCreate = () => {
  const [values, setValues] = useState({
    categories: [],
    loading: false,
    reload: false,
  });
  
  useEffect(() => {
    list().then(categories => setValues({
      ...values,
      categories
    }));
  }, [values.reload]);
  
  const schema = yup.object().shape({
    name: yup.string().required('Category is required.'),
  });
  
  const { register, handleSubmit, errors, setError, reset } = useForm({
    resolver: yupResolver(schema)
  });
  const onSubmit = async data => {
    setValues({
      ...values,
      loading: true
    });
    const res = await create(data);
    if (res.status === 200) {
      reset();
      toast(res.data.msg);
      await Router.push('/admin/category')
    } else if (res.status === 400) {
      setError(res.data.error.param, {
        message: res.data.error.msg,
      });
    } else if (res.status === 500) {
      toast(res.data.error.msg, 'warning');
    }
    setValues({
      ...values,
      loading: false,
      reload: !values.reload
    });
  };
  
  const onDelete = async data => {
    const res = await remove(data);
    if (res.status === 200) {
      toast(res.data.msg);
      await Router.push('/admin/category')
    } else if (res.status === 500) {
      toast(res.data.error.msg, 'warning');
    }
    setValues({
      ...values,
      loading: false,
      reload: !values.reload
    });
  };
  
  const onUpdate = async (data, cat_id) => {
    const res = await update(data, cat_id);
    setValues({
      ...values,
      loading: false,
      reload: !values.reload
    });
    return res;
  };
  
  const categoryForm = () => {
    return (
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-header">
              <h5><i className="fa fa-plus"></i> Add new category</h5>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                  <label htmlFor="name">Category</label>
                  <input type="text" className={cn('form-control', { 'is-invalid': errors.name })} id="name" name="name" ref={register}/>
                  {errors.name && <p className="invalid-feedback">{errors.name.message}</p>}
                </div>
                <div className="form-group">
                  <button className="btn btn-sm btn-outline-info float-right"> <i className="fa fa-check"></i> Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="col-md-12 mt-3">
          <CategoryIndex categories={values.categories} onUpdate={onUpdate} onDelete={onDelete}/>
        </div>
      </div>
    )
  };
  
  return (
    <Fragment>
      {categoryForm()}
    </Fragment>
  );
};

export default CategoryCreate;