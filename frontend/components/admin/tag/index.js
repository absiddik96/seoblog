import {useState} from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers';
import * as yup from "yup";
import cn from 'classnames';
import Router from "next/router";
import {toast} from "../../../actions/alert";

const TagIndex = ({ tags, onUpdate, onDelete }) => {
  const [editableTag, setEditableTag] = useState(null);
  const [deleteableTag, setDeleteableTag] = useState(null);
  const [open, setOpen] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const toggle = () => setOpen(!open);
  const deleteToggle = () => setDeleteModalOpen(!deleteModalOpen);
  
  const schema = yup.object().shape({
    name: yup.string().required('Tag is required.'),
  });
  
  const { register, handleSubmit, errors, setError, setValue, reset } = useForm({
    resolver: yupResolver(schema)
  });
  
  const onSubmit = async data => {
    const res = await onUpdate(data, editableTag._id)
    if (res.status === 200) {
      reset();
      toast(res.data.msg);
      await Router.push('/admin/tag');
      setOpen(false);
    } else if (res.status === 400) {
      setError(res.data.error.param, {
        message: res.data.error.msg,
      });
    } else if (res.status === 500) {
      toast(res.data.error.msg, 'warning');
    }
  };
  
  const makeEditable = async (tag) => {
    await setOpen(true);
    await setEditableTag(tag);
    await setValue('name', tag.name);
  };
  
  const makeDeleteable = async (tag) => {
    await setDeleteModalOpen(true);
    await setDeleteableTag(tag);
  };
  
  const deleteConfirm = async (tag) => {
    await setDeleteModalOpen(false);
    await onDelete(tag._id)
  };
  
  return (
    <div className="card">
      
      <Modal isOpen={open}>
        <ModalHeader>
          <i className="fa fa-edit"></i> Update Tag
        </ModalHeader>
        <ModalBody>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <label htmlFor="name">Tag</label>
              <input type="text" className={cn('form-control', { 'is-invalid': errors.name })} id="name" name="name" ref={register}/>
              {errors.name && <p className="invalid-feedback">{errors.name.message}</p>}
            </div>
            <div className="form-group float-right">
              <button type="button" className="btn btn-sm btn-outline-danger" onClick={toggle}> <i className="fa fa-times"></i> Close</button>{' '}
              <button className="btn btn-sm btn-outline-info"> <i className="fa fa-check"></i> Submit</button>
            </div>
          </form>
        </ModalBody>
      </Modal>
      
      <Modal isOpen={deleteModalOpen}>
        <ModalHeader>
          <i className="fa fa-trash"></i> Delete Confirmation?
        </ModalHeader>
        <ModalBody>
          <div className="form-group">
            <p>Are you sure you want to delete this?</p>
          </div>
          <div className="form-group float-right">
            <button type="button" className="btn btn-sm btn-outline-danger" onClick={deleteToggle}> <i className="fa fa-times"></i> No</button>{' '}
            <button className="btn btn-sm btn-outline-info" onClick={() => deleteConfirm(deleteableTag)}> <i className="fa fa-check"></i> Yes</button>
          </div>
        </ModalBody>
      </Modal>
      
      
      <div className="card-header">
        <h5> <i className="fa fa-list"></i> Tag List</h5>
      </div>
      <div className="card-body">
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {tags.length > 0 && tags.map((tag, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{tag.name}</td>
                  <td>
                    <button onClick={() => makeEditable(tag)} className="btn btn-outline-info">
                      <i className="fa fa-edit"></i>
                    </button>
                    {' '}
                    <button onClick={() => makeDeleteable(tag)} className="btn btn-outline-danger">
                      <i className="fa fa-trash"></i>
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};


export default TagIndex;