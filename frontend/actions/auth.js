import axios from 'axios';
import {API} from '../config';
import cookie from 'js-cookie';
import {toast} from '../actions/alert';

export const signup = async (formData) => {
  try {
    const res = await axios.post(`${API}/signup`, {
      name: formData.name,
      email: formData.email,
      password: formData.password,
    });
  
    return res;
  } catch (e) {
    return e.response;
  }
};

export const signin = async (formData) => {
  try {
    const res = await axios.post(`${API}/signin`, {
      email: formData.email,
      password: formData.password,
    });
    
    return res;
  } catch (e) {
    return e.response;
  }
};

export const setCookie = (key, value) => {
  if (process.browser) {
    cookie.set(key, value, {
      expires: 1
    });
  }
};

export const removeCookie = (key) => {
  if (process.browser) {
    cookie.remove(key, {
      expires: 1
    });
  }
};

export const getCookie = (key) => {
  if (process.browser) {
    return cookie.get(key);
  }
};

export const setLocalStorage = (key, value) => {
  if (process.browser) {
    localStorage.setItem(key, JSON.stringify(value));
  }
};

export const removeLocalStorage = (key) => {
  if (process.browser) {
    localStorage.removeItem(key);
  }
};

export const authenticate = (data, next) => {
  setCookie('token', data.token);
  setLocalStorage('user', data.user);
  next();
};

export const isAuth = () => {
  if (process.browser) {
    const checkCookie = getCookie('token');
    if (checkCookie) {
      if (localStorage.getItem('user')) {
        return JSON.parse(localStorage.getItem('user'));
      } else {
        return false;
      }
    }
  }
};

export const isAdmin = () => {
  if (isAuth() && isAuth().role === 1) {
    return isAuth();
  }
};

export const signOut = async (next) => {
  removeCookie('token');
  removeLocalStorage('user');
  next();
  toast('Sign out successfully');
  return await axios.get(`${API}/signout`);
};