import axios from 'axios';
import {API} from '../config';
import setAuthToken from "./setAuthToken";
import {getCookie} from "./auth";
import {toast} from "./alert";

// set auth token header auth
if (getCookie('token')) {
  setAuthToken(getCookie('token'));
}

export const list = async () => {
  try {
    const res = await axios.get(`${API}/category`);
    return res.data;
  } catch (e) {
    return e.response;
  }
};

export const create = async (formData) => {
  try {
    const res = await axios.post(`${API}/category/create`, {
      name: formData.name,
    });
    await list();
    return res;
  } catch (e) {
    return e.response;
  }
};

export const update = async (formData, cat_id) => {
  try {
    const res = await axios.put(`${API}/category/${cat_id}`, {
      name: formData.name,
    });
    await list();
    return res;
  } catch (e) {
    return e.response;
  }
};

export const remove = async (cat_id) => {
  try {
    const res = await axios.delete(`${API}/category/${cat_id}`);
    await list();
    toast(res.data.msg);
    return res;
  } catch (e) {
    return e.response;
  }
};