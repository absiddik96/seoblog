import axios from 'axios';
import {API} from '../config';
import setAuthToken from "./setAuthToken";
import {getCookie} from "./auth";
import {toast} from "./alert";

// set auth token header auth
if (getCookie('token')) {
  setAuthToken(getCookie('token'));
}

export const list = async () => {
  try {
    const res = await axios.get(`${API}/blog`);
    return res.data;
  } catch (e) {
    return e.response;
  }
};

//Add Post
export const addPost = formData => async dispatch => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const data = new FormData();
  data.append('title', formData.title);
  data.append('content', formData.content);
  data.append('image', formData.image);
  data.append('category', formData.category);

  try {
    const res = await axios.post('/api/posts', data, config);

    dispatch({
      type: ADD_POST,
      payload: res.data
    });

    dispatch(setAlert('Post Created', 'success'));
  } catch (err) {
    dispatch({
      type: POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status }
    });
  }
};

export const create = async (formData) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const data = await new FormData();
  await data.append('title', formData.title);
  await data.append('body', formData.body);
  await data.append('photo', formData.photo[0]);
  await data.append('categories', JSON.stringify(formData.categories));
  await data.append('tags', JSON.stringify(formData.tags));

  try {
    const res = await axios.post(`${API}/blog/create`, data, config);
    return res;
  } catch (e) {
    return e.response;
  }
};

export const update = async (formData, blog_id) => {
  try {
    const res = await axios.put(`${API}/blog/${blog_id}`, {
      name: formData.name,
    });
    await list();
    return res;
  } catch (e) {
    return e.response;
  }
};

export const remove = async (blog_id) => {
  try {
    const res = await axios.delete(`${API}/blog/${blog_id}`);
    await list();
    toast(res.data.msg);
    return res;
  } catch (e) {
    return e.response;
  }
};

export const getBlogs = async (data) => {
  try {
    const res = await axios.get(`${API}/blog/list`, {
      params: data,
    });
    return res.data;
  } catch (e) {
    return e.response;
  }
};

export const getSingleBlog = async (slug) => {
  try {
    const res = await axios.get(`${API}/blog/${slug}`);
    return res.data;
  } catch (e) {
    return e.response;
  }
};