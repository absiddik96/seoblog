const withCSS = require('@zeit/next-css');

module.exports = withCSS({
  publicRuntimeConfig: {
    APP_NAME: 'Blog',
    API: 'http://localhost:8000/api',
    PRODUCTION: false
  }
});