import Layout from "../components/Layout";
import Signin from "../components/auth/Signin";

const SignIn = () => {
  return (
    <Layout>
      <div className="container py-5">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <Signin />
          </div>
        </div>
      </div>
    </Layout>
  )
};

export default SignIn;