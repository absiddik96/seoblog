import React from 'react';
import Admin from "../../../components/auth/Admin";
import Layout from "../../../components/Layout";
import CategoryCreate from "../../../components/admin/category/Create";
import CategoryIndex from "../../../components/admin/category";

const Index = () => {
  return (
    <Layout>
      <Admin>
        <h4 className="text-secondary">Admin / Category</h4>
        <CategoryCreate />
      </Admin>
    </Layout>
  );
};

export default Index;