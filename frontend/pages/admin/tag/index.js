import React from 'react';
import Admin from "../../../components/auth/Admin";
import Layout from "../../../components/Layout";
import TagCreate from "../../../components/admin/tag/Create";

const Index = () => {
  return (
    <Layout>
      <Admin>
        <h4 className="text-secondary">Admin / Tag</h4>
        <TagCreate />
      </Admin>
    </Layout>
  );
};

export default Index;