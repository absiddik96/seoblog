import React from 'react';
import Admin from "../../../components/auth/Admin";
import Layout from "../../../components/Layout";
import BlogCreate from "../../../components/admin/blog/Create";

const Create = () => {
    return (
        <Layout>
            <Admin>
                <h4 className="text-secondary">Admin / Blog</h4>
                <BlogCreate />
            </Admin>
        </Layout>
    );
};

Create.getInitialProps = async ({ req }) => {
    return {};
};

export default Create;