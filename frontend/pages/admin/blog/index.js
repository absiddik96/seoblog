import React from 'react';
import Admin from "../../../components/auth/Admin";
import Layout from "../../../components/Layout";
import BlogIndex from "../../../components/admin/blog";

const Index = () => {
    return (
        <Layout>
            <Admin>
                <h4 className="text-secondary">Admin / Blog</h4>
                <BlogIndex/>
            </Admin>
        </Layout>
    );
};

export default Index;