import Layout from "../components/Layout";
import {getSingleBlog} from "../actions/blog";
import Head from "next/head";
import Link from "next/link";

const SingleBlog = (data) => {
  const {blog, relatedPost} = data.data;

  const head = () => (
    <Head>
      <title>{blog.mtitle}</title>
      <meta name="description" content={blog.mdesc}/>
    </Head>
  );

  return (
    <Layout>
      {head()}
      <div className="row">
        <div className="col-md-8">
          <div className="row">
            <div className="col-md-12 text-center">
              <img width="100%" src={blog.photo_url} alt=""/>
            </div>
            <div className="col-md-12 text-lightgray mt-5">
              <h2 className="text-info">{blog.title}</h2>
              <small>
                <i className="fa fa-user"/>
                {' '}{blog.postedBy.name}{' '}
                <i className="fa fa-calendar"/>
                {' '}{blog.publishedAt}{' '}
                <i className="fa fa-list-alt"/>
                {' '}{blog.categories.map(function (category, index) {
                  return (
                    <Link key={index} href={{ pathname: '/', query: { category: category.slug }}}>
                      <a className="text-decoration-none">{category.name}{index + 1 < blog.categories.length && ', '  }</a>
                    </Link>
                  )
                })}
                {' '}
                <i className="fa fa-tags"/>
                {' '}{blog.tags.map(function (tag, index) {
                  return (
                    <Link key={index} href={{ pathname: '/', query: { tag: tag.slug }}}>
                      <a className="text-decoration-none">{tag.name}{index + 1 < blog.tags.length && ', '  }</a>
                    </Link>
                  )
                })}
              </small>
              <hr/>
            </div>
            <div className="col-md-12 text-lightgray text-justify sun-editor-editable" dangerouslySetInnerHTML={{ __html: blog.body }}/>
          </div>
        </div>
        {/* /.col-md-8 */}
        <div className="col-md-4">
          <div className="card">
            <div className="card-body">
              <h3>Related Post</h3>
              <hr/>
              <ul className="">
                {relatedPost.map(function (post, index) {
                  return (
                    <li key={index} className="">
                      <Link href={`/${post.slug}`}><a className="text-info text-decoration-none">{post.title}</a></Link> <br/>
                      <small>
                        <i className="fa fa-user"/>
                        {' '}{post.postedBy.name}{' '}
                        <i className="fa fa-calendar"/>
                        {' '}{post.publishedAt}{' '} <br/>
                      </small>
                      <hr/>
                    </li>
                  )
                })}
                {/* /.list-group-item */}
              </ul>
              {/* /.list-group */}
            </div>
            {/* /.card-header */}
          </div>
          {/* /.card */}
        </div>
        {/* /.col-md-4 */}
      </div>
      {/* /.row */}
    </Layout>
  )
};

SingleBlog.getInitialProps = async ({query}) => {
  const data = await getSingleBlog(query.slug);
  return {data};
};

export default SingleBlog;