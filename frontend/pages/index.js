import Layout from "../components/Layout";
import {getBlogs} from "../actions/blog";
import Link from "next/link";
import Router from "next/router";
import classnames from 'classnames';
import {useState, useEffect, Fragment} from "react";

const Index = (props) => {
  let {blogs, numberOfBlogs} = props.res;
  let query = props.url.query;
  const [pagination, setPagination] = useState({
    perPage: 6,
    pageNumber: query.page ? parseInt(query.page) : 1,
    totalPage: 0
  });
  const {perPage, pageNumber, totalPage} = pagination;
  useEffect(() => {
    setPagination({
      ...pagination,
      totalPage: Math.ceil(numberOfBlogs/perPage)
    });
  }, []);

  const fetchBlogs = async (page) => {
    if (page >= 1 && page <= totalPage) {
      setPagination({
        ...pagination,
        pageNumber: page,
      });
      query.page = page;
      await Router.push({
        pathname: '',
        query: query,
      }).then(() => window.scrollTo(0, 0));
    }
  };

  const resetPageNumber = () => {
    setPagination({
      ...pagination,
      pageNumber: 1,
    });
  }

  const pagiElement = () => {
    if (numberOfBlogs > perPage) {
      return (
        <Fragment>
          <div className="col-md-6">
            <span onClick={() => fetchBlogs(pageNumber - 1)} className={classnames(`text-decoration-none float-right custom-link`, {"custom-disabled": pageNumber === 1})}><span className="h2">&larr;</span> Previous</span>
          </div>
          <div className="col-md-6">
            <span onClick={() => fetchBlogs(pageNumber + 1)} className={classnames(`text-decoration-none custom-link`, {"custom-disabled": pageNumber === totalPage})}>Next <span className="h2">&rarr;</span></span>
          </div>
        </Fragment>
      )
    }
  }

  return (
    <Layout>
      <div className="row">
        {blogs.map((blog, index) => {
          return (
            <Link href={`/${blog.slug}`}>
              <div key={index} className="col-12 custom-link">
              <div className="card mb-4 shadow">
                <div className="row">
                  <div className="col-md-4 pr-0">
                    <a className="text-info text-decoration-none">
                      <img className="card-img-top" src={blog.photo_thumb_url} alt={blog.title}/>
                    </a>
                  </div>
                  <div className="col-md-8 pl-0">
                    <div className="card-body text-lightgray">
                      <h4 className="card-title">
                        <a className="text-info text-decoration-none">
                          {blog.title}
                        </a>
                      </h4>
                      <small>
                        <i className="fa fa-user"/>
                        {' '}{blog.postedBy.name}{' '}
                        <i className="fa fa-calendar"/>
                        {' '}{blog.publishedAt}{' '} <br/>
                        <i className="fa fa-list-alt"/>
                        {' '}{blog.categories.map(function (category, index) {
                        return (
                          <Link key={index} href={{ pathname: '', query: { category: category.slug }}}>
                            <a onClick={resetPageNumber} className="text-decoration-none text-info">{category.name}{index + 1 < blog.categories.length && ', '  }</a>
                          </Link>
                        )
                      })}
                        {' '}
                        <i className="fa fa-tags"/>
                        {' '}{blog.tags.map(function (tag, index) {
                        return (
                          <Link key={index} href={{ pathname: '', query: { tag: tag.slug }}}>
                            <a onClick={resetPageNumber} className="text-decoration-none text-info">{tag.name}{index + 1 < blog.tags.length && ', '  }</a>
                          </Link>
                        )
                      })}
                      </small>
                      <hr/>
                      <small className="card-text">{blog.excerpt}</small>
                      {/*<Link href={`/${blog.slug}`}><h1 className="text-decoration-none float-right custom-link">&rarr;</h1></Link>*/}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </Link>
          )
        })}
        <div className="col-md-12">
          <div className="row">
            {pagiElement()}
          </div>
        </div>
      </div>
      {/* /.row */
      }
    </Layout>
  )
};

Index.getInitialProps = async ({query}) => {
  let data = query;
  data.page = query.page ? query.page : 1;
  const res = await getBlogs(data);
  return {res};
};

export default Index;