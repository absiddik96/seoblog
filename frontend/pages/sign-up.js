import Layout from "../components/Layout";
import Signup from "../components/auth/Signup";

const SignUp = () => {
  return (
    <Layout>
      <div className="container py-5">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <Signup />
          </div>
        </div>
      </div>
    </Layout>
  )
};

export default SignUp;