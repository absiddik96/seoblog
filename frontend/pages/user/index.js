import Layout from "../../components/Layout";
import Private from "../../components/auth/Private";

const UserIndex = () => {
  return (
    <Layout>
      <Private>
        <h3>User Dashboard</h3>
      </Private>
    </Layout>
  )
};

export default UserIndex;