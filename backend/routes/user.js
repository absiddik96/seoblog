const express = require('express');
const router = express.Router();
const { authMiddleware, requireSignin } = require('../middleware');
const {profile} = require('../controllers/user');

router.get('/profile', requireSignin, authMiddleware, profile);

module.exports = router;