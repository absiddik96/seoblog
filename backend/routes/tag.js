const express = require('express');
const router = express.Router();
const { adminMiddleware, requireSignin } = require('../middleware');
const { index, create, show, update, destroy } = require('../controllers/tag');

// validators
const { runValidation } = require('../validators');
const { tagCreateValidator } = require('../validators/tag');

router.get('/tag', requireSignin, adminMiddleware, index);
router.post('/tag/create', tagCreateValidator, runValidation, requireSignin, adminMiddleware, create);
router.get('/tag/:id', requireSignin, adminMiddleware, show);
router.put('/tag/:id', tagCreateValidator, runValidation, requireSignin, adminMiddleware, update);
router.delete('/tag/:id', requireSignin, adminMiddleware, destroy);

module.exports = router;