const express = require('express');
const router = express.Router();
const { adminMiddleware, requireSignin } = require('../middleware');
const { index, create, show, update, destroy } = require('../controllers/category');

// validators
const { runValidation } = require('../validators');
const { categoryCreateValidator } = require('../validators/category');

router.get('/category', requireSignin, adminMiddleware, index);
router.post('/category/create', categoryCreateValidator, runValidation, requireSignin, adminMiddleware, create);
router.get('/category/:id', requireSignin, adminMiddleware, show);
router.put('/category/:id', categoryCreateValidator, runValidation, requireSignin, adminMiddleware, update);
router.delete('/category/:id', requireSignin, adminMiddleware, destroy);

module.exports = router;