const express = require('express');
const router = express.Router();
const { adminMiddleware, requireSignin } = require('../middleware');
const { index, create, show, update, destroy, blogList } = require('../controllers/blog');
const multer = require('multer');

// validators
const { runValidation } = require('../validators');
const { blogCreateValidator } = require('../validators/blog');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/blog/');
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};
const upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 50,
        fileSize: 1024 * 1024
    },
    fileFilter: fileFilter
});

router.get('/blog/list', blogList);
router.get('/blog', requireSignin, adminMiddleware, index);
router.post('/blog/create', upload.single('photo'), blogCreateValidator, runValidation, requireSignin, adminMiddleware, create);
router.get('/blog/:slug', show);
router.put('/blog/:id', upload.single('photo'), blogCreateValidator, runValidation, requireSignin, adminMiddleware, update);
router.delete('/blog/:id', requireSignin, adminMiddleware, destroy);

module.exports = router;