const { check } = require('express-validator');

exports.blogCreateValidator = [
  check('title').not().isEmpty().withMessage('Title is required'),
  check('body').not().isEmpty().withMessage('body is required'),
  check('categories').not().isEmpty().withMessage('Categories is required'),
  check('tags').not().isEmpty().withMessage('Tags is required'),
];