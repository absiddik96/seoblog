const stripHtml = require("string-strip-html");
const slugify = require("slugify");
const Blog = require('../models/blog');
const Category = require('../models/category');
const Tag = require('../models/tag');
const {smartTrim} = require('../helpers');
const fs = require('fs');
const sharp = require('sharp');
const {getEmbeddedBody, uploadBodyImage} = require("../helpers");

exports.index = async (req, res) => {
  try {
    const blogs = await Blog.find({}).sort({createdAt: -1});
    return res.json(blogs);
  } catch (e) {
    return res.status(500).json({error: e})
  }
};

exports.create = async (req, res) => {
  let {title, body} = req.body;
  let slug = slugify(title).toLowerCase();
  let mtitle = `${title} | ${process.env.APP_NAME}`;
  let mdesc = stripHtml(body.substring(0, 160)).result;
  let photo = req.file.path;
  let postedBy = req.user._id;
  let categories = req.body.categories.replace(/["]+/g, '').split(",");
  let tags = req.body.tags.replace(/["]+/g, '').split(",");
  let excerpt = smartTrim(body, 320, ' ', ' ...');
  body = await uploadBodyImage(body);

  try {
    if (photo) {
      sharp(req.file.path).resize(340, 240).toFile('uploads/blog/thumb/' + req.file.filename, (err, resizeImage) => {
        if (err) {
          console.log(err);
        } else {
          console.log(resizeImage);
        }
      });
    }
    const isExists = await Blog.find({slug: {$regex: `^${slug}`}});
    if (isExists.length) slug = slug + '-' + isExists.length;

    let blog = new Blog({title, body, slug, mtitle, mdesc, photo, postedBy, categories, tags, excerpt});
    await blog.save();
    return res.json({msg: 'Blog created successfully'});
  } catch (e) {
    return res.status(500).json({error: e})
  }
};

exports.show = async (req, res) => {
  const slug = req.params.slug;
  try {
    const blog = await Blog.findOne({slug: slug})
      .populate('categories', '_id name slug')
      .populate('tags', '_id name slug')
      .populate('postedBy', '_id name username');
    if (!blog) return res.status(404).json({
      error: 'No data found'
    });
    const relatedPost = await Blog.find({ categories: {$in: blog.categories}})
      .select('_id title slug createdAt updatedAt photo_url postedBy publishedAt')
      .populate('postedBy', '_id name username');

    blog.body = await getEmbeddedBody(blog.body)

    return res.json({
      blog,
      relatedPost
    });
  } catch (e) {
    return res.status(500).json({error: e})
  }
};

exports.update = async (req, res) => {
  const blog_id = req.params.id;
  const {title, body} = req.body;
  let slug = slugify(title).toLowerCase();
  let mtitle = `${title} | ${process.env.APP_NAME}`;
  let mdesc = stripHtml(body.substring(0, 160)).result;
  let photo = req.file ? req.file.path : null;
  let postedBy = req.user._id;
  let categories = req.body.categories.replace(/["]+/g, '').split(",");
  let tags = req.body.tags.replace(/["]+/g, '').split(",");
  let excerpt = smartTrim(body, 320, ' ', ' ...');

  try {
    const blog = await Blog.findById(blog_id);
    if (!blog) return res.status(404).json({
      error: 'No data found'
    });

    const isExists = await Blog.find({slug: {$regex: `^${slug}`}, _id: {$ne: blog_id}});
    if (isExists.length) slug = slug + '-' + isExists.length;

    let updateData = {title, body, slug, mtitle, mdesc, postedBy, categories, tags, excerpt};
    if (photo) {
      updateData.photo = photo
    } else {
      updateData.photo = blog.photo;
    }

    await blog.overwrite(updateData);
    await blog.save();
    return res.json({msg: 'Blog updated successfully'});
  } catch (e) {
    return res.status(500).json({error: e});
  }
};

exports.destroy = async (req, res) => {
  const blog_id = req.params.id;
  try {
    const blog = await Blog.findById(blog_id);
    if (!blog) return res.status(404).json({
      error: 'No data found'
    });
    await fs.unlink(blog.photo, function () {});
    await blog.remove();
    return res.json({msg: 'Blog deleted successfully'});
  } catch (e) {
    return res.status(500).json({error: e})
  }
};

exports.blogList = async (req, res) => {
  try {
    let queries = {};
    if (req.query.category){
      const category = await Category.findOne({slug: req.query.category});
      queries.categories = category._id;
    }
    if (req.query.tag){
      const tag = await Tag.findOne({slug: req.query.tag});
      queries.tags = tag._id;
    }
    const numberOfBlogs = await Blog.find(queries).count();
    const blogs = await Blog.find(queries)
      .select('_id title slug excerpt photo photo_url photo_thumb_url categories tags postedBy publishedAt createdAt updatedAt')
      .populate('categories', '_id name slug')
      .populate('tags', '_id name slug')
      .populate('postedBy', '_id name username')
      .sort({createdAt: -1})
      .skip(6 * (req.query.page - 1))
      .limit(6);
    return res.json({
      blogs,
      numberOfBlogs
    });
  } catch (e) {
    return res.status(500).json({error: e})
  }
};