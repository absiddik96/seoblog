const User = require('../models/user');
const shortid = require('shortid');
const jwt = require('jsonwebtoken');

exports.signup = async (req, res) => {
  const { name, email, password } = req.body;
  const user = await User.findOne({ email: email });
  if (user) return res.status(400).json({
    error: {
      param: 'email',
      msg: 'Email is taken'
    }
  });
  
  let username = shortid.generate();
  let profile = `${process.env.CLIENT_URL}/profile/${username}`;
  
  let newUser = await new User({ name, email, password, profile, username });
  try {
    await newUser.save();
    return res.status(200).json({ msg: 'SignUp success! Please SignIn' })
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.signin = (req, res) => {
  const { email, password } = req.body;
  // check if user exist
  User.findOne({ email }).exec((err, user) => {
    if (err || !user) {
      return res.status(400).json({
        error: {
          param: 'email',
          msg: `those credential doesn't exists in our record.`
        }
      });
    }
    // authenticate
    if (!user.authenticate(password)) {
      return res.status(400).json({
        error: {
          param: 'email',
          msg: `those credential doesn't exists in our record.`
        }
      });
    }
    // generate a token and send to client
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '1d' });
    
    res.cookie('token', token, { expiresIn: '1d' });
    const { _id, username, name, email, role } = user;
    return res.json({
      token,
      user: { _id, username, name, email, role }
    });
  });
};

exports.signout = (req, res) => {
  res.clearCookie('token');
  return res.json({ msg: 'Signout successfully' });
};
