const slugify = require("slugify");
const Tag = require('../models/tag');

exports.index = async (req, res) => {
  try {
    const tags = await Tag.find({}).sort({ name: 1 });
    return res.json(tags);
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.create = async (req, res) => {
  const { name } = req.body;
  let slug = slugify(name).toLowerCase();
  
  try {
    const isExists = await Tag.findOne({ slug });
    if (isExists) return res.status(400).json({
      error: {
        param: 'name',
        msg: 'Tag already exists'
      }
    });
    
    let tag = new Tag({ name, slug });
    await tag.save();
    return res.json({ msg: 'Tag created successfully' });
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.show = async (req, res) => {
  const cat_id = req.params.id;
  try {
    const tag = await Tag.findById(cat_id);
    if (!tag) return res.status(404).json({
      error: 'No data found'
    });
    return res.json(tag);
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.update = async (req, res) => {
  const cat_id = req.params.id;
  const { name } = req.body;
  let slug = slugify(name).toLowerCase();
  
  try {
    const tag = await Tag.findById(cat_id);
    if (!tag) return res.status(404).json({
      error: 'No data found'
    });
    
    const isExists = await Tag.findOne({ slug, _id: { $ne: cat_id } });
    if (isExists) return res.status(400).json({
      error: {
        param: 'name',
        msg: 'Tag already exists'
      }
    });
    
    await tag.overwrite({ name, slug });
    await tag.save();
    return res.json({ msg: 'Tag updated successfully' });
  } catch (e) {
    return res.status(500).json({ error: e });
  }
};

exports.destroy = async (req, res) => {
  const cat_id = req.params.id;
  try {
    const tag = await Tag.findById(cat_id);
    if (!tag) return res.status(404).json({
      error: 'No data found'
    });
    await tag.remove();
    return res.json({ msg: 'Tag deleted successfully' });
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};
