const slugify = require("slugify");
const Category = require('../models/category');

exports.index = async (req, res) => {
  try {
    const categories = await Category.find({}).sort({ name: 1 });
    return res.json(categories);
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.create = async (req, res) => {
  const { name } = req.body;
  let slug = slugify(name).toLowerCase();
  
  try {
    const isExists = await Category.findOne({ slug });
    if (isExists) return res.status(400).json({
      error: {
        param: 'name',
        msg: 'Category already exists'
      }
    });
    
    let category = new Category({ name, slug });
    await category.save();
    return res.json({ msg: 'Category created successfully' });
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.show = async (req, res) => {
  const cat_id = req.params.id;
  try {
    const category = await Category.findById(cat_id);
    if (!category) return res.status(404).json({
      error: 'No data found'
    });
    return res.json(category);
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};

exports.update = async (req, res) => {
  const cat_id = req.params.id;
  const { name } = req.body;
  let slug = slugify(name).toLowerCase();
  
  try {
    const category = await Category.findById(cat_id);
    if (!category) return res.status(404).json({
      error: 'No data found'
    });
    
    const isExists = await Category.findOne({ slug, _id: { $ne: cat_id } });
    if (isExists) return res.status(400).json({
      error: {
        param: 'name',
        msg: 'Category already exists'
      }
    });
    
    await category.overwrite({ name, slug });
    await category.save();
    return res.json({ msg: 'Category updated successfully' });
  } catch (e) {
    return res.status(500).json({ error: e });
  }
};

exports.destroy = async (req, res) => {
  const cat_id = req.params.id;
  try {
    const category = await Category.findById(cat_id);
    if (!category) return res.status(404).json({
      error: 'No data found'
    });
    await category.remove();
    return res.json({ msg: 'Category deleted successfully' });
  } catch (e) {
    return res.status(500).json({ error: e })
  }
};
