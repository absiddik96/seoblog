const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

// bring routes
const blogRoute = require('./routes/blog');
const authRoute = require('./routes/auth');
const userRoute = require('./routes/user');
const categoryRoute = require('./routes/category');
const tagRoute = require('./routes/tag');

// app
const app = express();

// database
try {
  mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });
  console.log('Database connected')
} catch (e) {
  console.log('Database not connected')
}

// middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
//Ignore path Middleware
app.use('/uploads', express.static('uploads'));

// cors
if (process.env.NODE_ENV === 'development')
  app.use(cors({ origin: `${process.env.CLIENT_URL}` }));


// routes middleware
app.use('/api', blogRoute);
app.use('/api', authRoute);
app.use('/api', userRoute);
app.use('/api', categoryRoute);
app.use('/api', tagRoute);

// port
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});