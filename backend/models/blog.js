const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;
const moment = require('moment');

const blogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
      min: 3,
      max: 160,
      required: true
    },
    slug: {
      type: String,
      unique: true,
      index: true
    },
    body: {
      type: {},
      required: true,
      min: 200,
      max: 2000000
    },
    excerpt: {
      type: String,
      max: 1000
    },
    mtitle: {
      type: String
    },
    mdesc: {
      type: String
    },
    photo: {
      type: String,
    },
    categories: [{ type: ObjectId, ref: 'Category', required: true }],
    tags: [{ type: ObjectId, ref: 'Tag', required: true }],
    postedBy: {
      type: ObjectId,
      ref: 'User'
    }
  },
  { timestamps: true }
);

blogSchema.set('toObject', { virtuals: true });
blogSchema.set('toJSON', { virtuals: true });

blogSchema.virtual('photo_url').get(function () {
  return process.env.APP_URL+'/'+this.photo;
});

blogSchema.virtual('photo_thumb_url').get(function () {
  let filename = this.photo;
  let filenameArray = filename.split('/');
  return process.env.APP_URL+'/uploads/blog/thumb/'+filenameArray[filenameArray.length - 1];
});

blogSchema.virtual('publishedAt').get(function () {
  return moment(this.createdAt).format('LL')
});

module.exports = mongoose.model('Blog', blogSchema);