const expressJwt = require('express-jwt');
const User = require('../models/user');

exports.requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  algorithms: ['HS256']
});

exports.authMiddleware =  async (req, res, next) => {
  const authUserId = req.user._id
  try {
    const user = await User.findById(authUserId);
    if (!user) {
      return res.status(400).json({ error: {
          param: null,
          msg: 'User not found'
        }})
    }
    req.profile = user;
    next();
  } catch (err) {
    return res.status(500).json({error: 'Something wrong!!! Please try again'});
  }
};

exports.adminMiddleware =  async (req, res, next) => {
  const adminUserId = req.user._id
  try {
    const user = await User.findById(adminUserId);
    if (!user) {
      return res.status(400).json({ error: {
          param: null,
          msg: 'User not found'
        }})
    }
    if (user.role !== 1) {
      return res.status(401).json({error: 'Access denied'});
    }
    req.profile = user;
    next();
  } catch (err) {
    return res.status(500).json({error: 'Something wrong!!! Please try again'});
  }
};