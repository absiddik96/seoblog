const stripHtml = require("string-strip-html");
const cheerio = require('cheerio');
const mime = require('mime');
const fs = require('fs');

exports.smartTrim = (str, length, delim, appendix) => {
    str = stripHtml(str).result;
    if (str.length <= length) return str;

    var trimmedStr = str.substr(0, length + delim.length);

    var lastDelimIndex = trimmedStr.lastIndexOf(delim);
    if (lastDelimIndex >= 0) trimmedStr = trimmedStr.substr(0, lastDelimIndex);

    if (trimmedStr) trimmedStr += appendix;
    return trimmedStr;
};

exports.getEmbeddedBody = (body) => {
    const $ = cheerio.load(body);
    $('img').each(function(i, elem) {
      let src = $(this).attr('src');
      $(this).attr('src', process.env.APP_URL + '/' + src);
    });
    return  $('body').html();
};

exports.uploadBodyImage = async (body) => {
    console.log(body)
    const $ = cheerio.load(body);
    await $('img').each( async function(i, elem) {
        let src = $(this).attr('src');
        let new_src = await uploadBase64Img(src);
        $(this).attr('src', new_src);
    });
    return  $('body').html();
};

const uploadBase64Img = (src) =>  {
    let matches = src.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    let response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    let decodedImg = response;
    let imageBuffer = decodedImg.data;
    let type = decodedImg.type;
    let extension = mime.getExtension(type);
    let fileName = Date.now() + "." + extension;
    try {
        let file_path = "./uploads/blog/article/" + fileName;
        fs.writeFileSync(file_path, imageBuffer, 'utf8');
        return file_path;
    } catch (e) {
        return null;
    }
};